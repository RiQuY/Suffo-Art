﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SuffoArt.Models
{
    public class MediaBD
    {

        SuffoArtEntities db = new SuffoArtEntities();

        public bool AñadirMedia(MediaViewModel datos)
        {
            bool exito;
            TipoMedia tipoMediaBD = (from tipoMedia in db.TipoMedia
                                     where tipoMedia.Id == datos.Tipo
                                     select tipoMedia)
                                    .SingleOrDefault();

            Media media = new Media
            {
                Titulo = datos.Titulo,
                TipoMedia = tipoMediaBD,
                Ubicacion = Path.GetFileName(datos.Ubicacion.FileName),
                Descripcion = datos.Descripcion
            };

            db.Media.Add(media);
            // Submit the changes to the database.
            try
            {
                db.SaveChanges();
                exito = true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                exito = false;
            }

            return exito;
        }

        public List<Media> GetIlustracionesPaginated(int numRegistro)
        {
            int numeroElementos = 8;

            List<Media> ilustracionesBD = null;
            TipoMedia tipoMediaBD = (from tipoMedia in db.TipoMedia
                                     where tipoMedia.Nombre == "Imagen"
                                     select tipoMedia)
                                    .SingleOrDefault();
            if (tipoMediaBD != null)
            {
                ilustracionesBD = (from media in db.Media
                                   where media.Tipo == tipoMediaBD.Id
                                   select media)
                                  .OrderByDescending(registroMedia => registroMedia.FechaCreacion)
                                  .Skip(numRegistro)
                                  .Take(numeroElementos)
                                  .ToList();
            }
            
            return ilustracionesBD;
        }

        public List<Media> GetAnimacionesPaginated(int numRegistro)
        {
            int numeroElementos = 2;

            List<Media> animacionesBD = null;
            TipoMedia tipoMediaBD = (from tipoMedia in db.TipoMedia
                                     where tipoMedia.Nombre == "Vídeo"
                                     select tipoMedia)
                                    .SingleOrDefault();
            if (tipoMediaBD != null)
            {
                animacionesBD = (from media in db.Media
                                 where media.Tipo == tipoMediaBD.Id
                                 select media)
                                .OrderByDescending(registroMedia => registroMedia.FechaCreacion)
                                .Skip(numRegistro)
                                .Take(numeroElementos)
                                .ToList();
            }

            return animacionesBD;
        }

        public Media GetMedia(int id)
        {
            Media mediaDB = (from media in db.Media
                             where media.Id == id
                             select media)
                            .SingleOrDefault();

            return mediaDB;
        }
        public Media GetMediaPorTitulo(string titulo)
        {
            Media mediaDB = (from media in db.Media
                             where media.Titulo == titulo
                             select media)
                            .SingleOrDefault();

            return mediaDB;
        }

        public bool EditarMedia(Media mediaEdit)
        {
            bool exito;

            Media mediaDB = (from media in db.Media
                             where media.Id == mediaEdit.Id
                             select media)
                            .SingleOrDefault();

            if (mediaDB != null)
            {
                mediaDB.Titulo = mediaEdit.Titulo;
                mediaDB.Descripcion = mediaEdit.Descripcion;

                try
                {
                    db.SaveChanges();
                    exito = true;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);
                    exito = false;
                }
            }
            else
            {
                exito = false;
            }

            return exito;
        }

        public bool DeleteMedia(Media mediaDel)
        {
            bool exito;

            Media mediaDB = db.Media.Where(d => d.Id == mediaDel.Id).First();
            db.Media.Remove(mediaDB);

            try
            {
                db.SaveChanges();
                exito = true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                exito = false;
            }

            return exito;
        }
    }
}