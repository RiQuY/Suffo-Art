﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SuffoArt.Models
{
    public class LoginBD
    {
        SuffoArtEntities db = new SuffoArtEntities();

        public bool ComprobarLogin(LoginViewModel datos)
        {
            bool resultado = false;

            //GeneratePassword(datos.Password);

            Usuario usuarioBD = (from usuario in db.Usuario
                                 where usuario.Username == datos.Username
                                 select usuario)
                                .SingleOrDefault();

            if (SecurePasswordHasher.Verify(datos.Password, usuarioBD.Password))
            {
                resultado = true;
            }

            return resultado;
        }

        //private void GeneratePassword(string pass)
        //{
        //    string hashedPassword = SecurePasswordHasher.Hash(pass);

        //    // Opción update
        //    Usuario usuario1 = (from usuario in db.Usuario
        //                        where usuario.Username == "pSuffo"
        //                        select usuario)
        //                        .SingleOrDefault();
        //    usuario1.Password = hashedPassword;
        //    */

        //    // Opción insert
        //    Usuario usuario2 = new Usuario
        //    {
        //        Username = "pSuffo",
        //        Password = hashedPassword
        //    };
        //    db.Usuario.Add(usuario2);

        //    // Submit the changes to the database.
        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //    }
        //}
    }
}