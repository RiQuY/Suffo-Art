﻿using SuffoArt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuffoArt.Controllers
{
    public class PrincipalController : Controller
    {
        MediaBD mediaModel = new MediaBD();

        // GET: Inicio
        public ActionResult Inicio()
        {
            return View();
        }

        // GET: Bio
        //public ActionResult Bio()
        //{
        //    return View();
        //}

        // GET: Animacion
        public ActionResult Animacion()
        {
            if (Session["NumRegistroAnimaciones"] == null)
            {
                Session["NumRegistroAnimaciones"] = 0;
            }
            List<Media> animaciones = mediaModel.GetAnimacionesPaginated((int)Session["NumRegistroAnimaciones"]);

            return View(animaciones);
        }

        // GET: ResetNumPaginaAnimacion()
        public ActionResult ResetNumPaginaAnimacion()
        {
            Session["NumRegistroAnimaciones"] = null;
            return RedirectToAction("Animacion");
        }

        // GET: SiguientePaginaAdminAnimacion()
        public ActionResult SiguientePaginaAnimacion()
        {
            if (Session["NumRegistroAnimaciones"] == null)
            {
                Session["NumRegistroAnimaciones"] = 0;
            }
            int numRegistro = (int)Session["NumRegistroAnimaciones"];
            numRegistro += 2;
            Session["NumRegistroAnimaciones"] = numRegistro;
            return RedirectToAction("Animacion");
        }

        // GET: Ilustracion
        public ActionResult Ilustracion()
        {
            if (Session["NumRegistroIlustraciones"] == null)
            {
                Session["NumRegistroIlustraciones"] = 0;
            }
            List<Media> ilustraciones = mediaModel.GetIlustracionesPaginated((int)Session["NumRegistroIlustraciones"]);

            return View(ilustraciones);
        }

        // GET: ResetNumPaginaIlustracion()
        public ActionResult ResetNumPaginaIlustracion()
        {
            Session["NumRegistroIlustraciones"] = null;
            return RedirectToAction("Ilustracion");
        }

        // GET: SiguientePaginaIlustraciones()
        public ActionResult SiguientePaginaIlustracion()
        {
            if (Session["NumRegistroIlustraciones"] == null)
            {
                Session["NumRegistroIlustraciones"] = 0;
            }
            int numRegistro = (int)Session["NumRegistroIlustraciones"];
            numRegistro += 8;
            Session["NumRegistroIlustraciones"] = numRegistro;
            return RedirectToAction("Ilustracion");
        }

        // GET: Blog
        //public ActionResult Blog()
        //{
        //    return View();
        //}

        // GET: Contacto
        public ActionResult Contacto()
        {
            return View();
        }
    }
}